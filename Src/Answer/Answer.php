<?php
namespace App\Answer;
include ("../../vendor/autoload.php");
use App\Utility\Utility;
use PDO;
/*session_start();*/
class Answer
{
 private $ans;
 private $question_id;

 public function setAnswer($data = "")
 {
     $this->ans = $data['ans'];
     $this->question_id = $data['question_id'];
     return $this;
 }
 public function viewAnswer(){

     $pdo = new PDO('mysql:host=localhost;dbname=amarprosno', 'root', '');
     $query = "SELECT answers . * , users.user_name
 FROM answers LEFT JOIN users ON answers.user_id= users.id WHERE answers.question_id = 1";

 $stmt = $pdo->prepare($query);
     $stmt-> execute();
     $temp = $stmt->fetch();
     return $temp;
 }

    public function getAnswer(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=amarprosno', 'root', '');
            $query = 'INSERT INTO answers(user_id,question_id,answer) VALUES(:uid,:qid,:ansdetail)';
            $stmt = $pdo->prepare($query);
            $data = [
                ':uid'=>$_SESSION['userid']['id'],
                ':qid'=>$this->question_id,
                'ansdetail' => $this->ans,
            ];
            $status = $stmt->execute($data);
            if($status){
                $_SESSION['Message'] = "<h1>Your Answer is</h1>";
                header("location:Create.php");
            }
            else{
                echo "<h1>Opps Something wrong</h1>";

            }

        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
}