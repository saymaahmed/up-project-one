<?php
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>TODO supply a title</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
<div class="container>
    <div class="row6">
        <div class="col-xs-12">
            <div class="box3">
                <h2>User Registration</h2>
            </div>
        </div>
    </div>
    <div class="row7">
        <div class="col-xs-12">
            <form class="form-horizontal"  action="Store.php" method="post" id="myForm">
                <div class="form-group">
                    <label for="inputfield1" class="col-xs-2 control-label">User name</label>
                    <div class="controls">
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="inputfield1" name="inputfield1" placeholder="user-name" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputfield2" class="col-xs-2 control-label">Email</label>

                    <div class="col-xs-6">
                        <input type="email" class="form-control" id="inputfield2" name="inputfield2" placeholder="email" required>
                    </div>

                </div>
                <div class="form-group">
                    <label for="inputfield3" class="col-xs-2 control-label">Password</label>

                    <div class="col-xs-6">
                        <input type="password" class="form-control" id="inputfield3" name="inputfield3" placeholder="password" required>
                    </div>

                </div>
                <div class="form-group">
                    <label for="inputfield4" class="col-xs-2 control-label">Confirm_password</label>

                    <div class="col-xs-6">
                        <input type="password" class="form-control" id="inputfield4" name="inputfield4" placeholder="Confirm_password" required>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-xs-8">
                        <center><input type="submit" class="btn btn-primary" id="inputfield6" name="inputfield5" value="Register"></center>
                    </div>

                </div>

            </form>
        </div>
    </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js "></script>
<script>
    $(document).ready(function(){
        $("#myForm") .validate({
            rules: {
                inputfield1: {
                    required: true
                },

                inputfield2: {
                    required: true
                },
                inputfield3: {
                    required: true,
                    minlength: 4
                },
                inputfield4: {
                    required: true,
                    minlength: 4,
                    equalTo: "#inputfield3"
                },
                inputfield5: {
                    required: true
                },
            }
        });
    });
</script>


</body>
</html>


