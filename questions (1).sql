-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2017 at 12:57 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amarprosno`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_title` varchar(255) NOT NULL,
  `question_detail` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `user_id`, `question_title`, `question_detail`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Responsive Spritesheet animation Issues', 'I am following the following tutorials links, It works fine as long as I am using same image, as soon as I replace the image, its animation behaves abruptly (sliding from top to bottom)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 'How to convert the input from /dev/input/eventX?', 'I would like to read the touchpad input from /dev/input/eventX. Although many questions ( Get mouse deltas using Python! (in Linux) ) answer how to do so, they do not quite answer how to convert the data obtained into sensible output.\r\n\r\nIt is not obvious to me how to convert them. And also how does one write into a eventX file. Please help me!', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 'Processing Multiple files in hadoop python', 'I have a scenario where text delimited files arrives from different servers(around 10) to hadoop system every 30 minutes.\r\n\r\nEach file has around 2.5 million records and may not arrive at the same time, I am looking for an approach where these file can be processed every 30 minutes.\r\n\r\nMy questions are:\r\n\r\n1.How to handle files arriving at different times?\r\n2.I want the data to be aggregated across 10 files. Should such large files be combined or processed separately?\r\nI want this solution to be implemented in python but solutions using any tools/techniques in hadoop would be appreciated.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 'Copy value from one Series / DataFrame to Another only when is finite and nonzero and preserve existing value when not finite and zero', 'I have a Series which has values and zeros, which holds current readings, at a given point in time. The zero indicates there is not new reading for a given set of readings. I want to maintain another Series with the last known value.\r\n\r\nFor this you should be able to copy nonzero values from the the series holding the current values to the one with the last known values and also preserve the values in the last known values when the reading of the current value is zero.\r\n\r\nHow can I maintain these data structures as Series and Data Frames?', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, 'Execute action when SMS is received in UWP app', 'I would like to programmatically set the ringtone in a UWP app. However, as I read in How to programmatically get / change ringtone and alert tones, that does not seem to be possible.\r\n\r\nI read the How to use the save ringtone task for Windows Phone 8, but this is not what I am looking for.\r\n\r\nIs there a way to intercept the SMS received event and execute an action when an SMS is received (I do not need to read the SMS, I just want to play a custom tone when a message is received)?\r\n\r\nI do not want to create an SMS app.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, 'Not to scaffolding with Application Insights packages when creating a new Web projects in Visual Studio 2015', 'When creating a new Web project (WCF, MVC, Web API), Visual Studio always introduces ApplicationInsights packages which I don\'t use. I have to remove them one by one through NuGet manager. Is there a setting somewhere to stop such behavior of Visual Studio?', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
